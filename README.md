# WhatsApp dark theme
**This theme is no longer being developed. Whatsapp has announced to develop a dark theme for WhatsWeb.**</span>

## Preview
![Chat_history](/uploads/f2e43e10de4dc31cfed81563450cdd3e/Chat_history.PNG)
![chat_emoticons](/uploads/18d5e6bc76e59f97eba0ce48a07df295/chat_emoticons.PNG)

## Installation
Before trying to install the dark theme,
make sure you have the windows version from the [WhatsApp](https://www.whatsapp.com/download/) page.
The Microsoft Store version is not supported.

1. Execute program as administrator
2. Run the install command
3. Start/restart WhatsApp

## Customization
To customize the theme go to the style folder and edit the "css.css" file. 
All you need to do is change the colors in the root area.

## Issues
If you have found a bug or want to suggest an improvement please open a [new issue](https://gitlab.com/faith79/whatsapp-dark-theme/issues/new).

## Library used
*  [ASAR.NET](https://github.com/Jiiks/asar.net)