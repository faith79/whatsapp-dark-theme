﻿using System;

namespace WhatsApp_Dark_Theme
{
	internal static class Program
	{
		public static void Main(string[] args)
		{
			var exit = false;
			Console.WriteLine("WhatsApp dark theme created by Faith79");
			do
			{
				string readLine;
				Installer installer;
				Console.WriteLine("Commands: install, uninstall, exit");
				readLine = Console.ReadLine();

				switch (readLine?.ToLower())
				{
					case "install":
					{
						installer = new Installer();
						installer.Install();
						break;
					}
					case "uninstall":
						installer = new Installer();
						installer.Uninstall();
						break;
					case "exit":
						exit = true;
						break;
				}
			} while (!exit);
		}
	}
}