using System;
using System.Collections.Generic;
using System.IO;
using asardotnet;


namespace WhatsApp_Dark_Theme
{
	public class Installer
	{
		private readonly string whatsAppPath;
		private string resourcesPath;
		private string filename;
		private string asarPath;

		public Installer()
		{
			whatsAppPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\WhatsApp";
			init();
		}

		private void init()
		{
			if (Directory.Exists(whatsAppPath))
			{
				int latestVersion;
				string[] directories;
				Console.WriteLine("Searching for latest version");
				directories = Directory.GetDirectories(whatsAppPath);
				latestVersion = LatestVersion(directories);
				foreach (string directory in directories)
				{
					string filename = Path.GetFileName(directory);
					if (filename != null && filename.Replace(".", "").EndsWith(latestVersion.ToString()))
					{
						this.filename = filename;
						resourcesPath = directory + @"\resources";
						asarPath = directory + @"\resources\app.asar";
					}
				}
			} else
			{
				Console.Error.WriteLine("Couldn't find WhatsApp. The store version isn't supported.");
			}
		}

		public void Install()
		{
			Console.WriteLine("Latest version found: " + filename);
			Console.WriteLine("Installing");
			Console.Write("Extracting CSS file ");
			ExtractCSSMFile(asarPath, resourcesPath + @"\");
			RenameCSSMFile(resourcesPath + @"\cssm.css");
			// ExtractCSSFile(asarPath, resourcesPath);
			Console.WriteLine("\nDone");
			Console.Write("Extracting dark theme ");
			ExtractDarkTheme(AppDomain.CurrentDomain.BaseDirectory + @"\style\style.css");
			Console.WriteLine("\nDone");
			Console.Write("Injecting dark theme ");
			Inject();
			Console.WriteLine("\nFinished");
		}

		private void ExtractCSSMFile(string inputPath, string outputPath)
		{
			var arch = new AsarArchive(inputPath);
			var extractor = new AsarExtractor();
			extractor.ExtractAll(arch, outputPath);
		}

		private void RenameCSSMFile(string inputPath)
		{
			var fileInfo = new FileInfo(inputPath);
			fileInfo.MoveTo(fileInfo.Directory?.FullName + "\\" + "css" + fileInfo.Extension);
		}


		public void Uninstall()
		{
			var readAllBytes = ReadBytes(asarPath);
			Console.Write("Removing all files ");

			if (File.Exists(resourcesPath + @"\css.css"))
				File.Delete(resourcesPath + @"\css.css");

			using (var fileStream = new FileStream(asarPath, FileMode.Open))
			{
				ModifyBytes(readAllBytes, @"..\css.css", @"./cssm.css");
				foreach (byte character in readAllBytes)
					fileStream.WriteByte(character);
			}

			Console.WriteLine("\nFinished");
		}


		[Obsolete(message: "Use ExtractCSSMFile() ")]
		private void ExtractCSSFile(string inputPath, string outputPath)
		{
			int lines = GetLinesOfFile(inputPath);
			var lineCount = 0;
			using (var reader = new StreamReader(inputPath))
			{
				using (var writer = new StreamWriter(new FileStream(outputPath + @"\css.css", FileMode.Create)))
				{
					using (var progress = new ProgressBar())
					{
						var start = false;
						string currentLine;
						while ((currentLine = reader.ReadLine()) != null)
						{
							var startOfFileNumber = 0;
							var words = currentLine.Split(' ');
							if (currentLine.EndsWith("{"))
							{
								foreach (char letter in currentLine.ToCharArray())
								{
									if (letter == 65533)
										startOfFileNumber++;
									else if (letter == 74 && startOfFileNumber == 1)
										startOfFileNumber++;
									else if (letter == 65533 && startOfFileNumber == 2)
										startOfFileNumber++;
									else if (letter == 11 && startOfFileNumber == 3)
										startOfFileNumber++;
									else if (letter == 46 && startOfFileNumber == 4)
									{
										start = true;
									} else
										startOfFileNumber = 0;
								}
							}

							if (start)
							{
								var end = 0;
								foreach (string word in words)
								{
									foreach (char letter in word.ToCharArray())
									{
										if (letter == 65533)
											end++;
										else if (letter == 80 && end == 1)
											end++;
										else if (letter == 78 && end == 2)
											end++;
										else if (letter == 71 && end == 3)
											start = false;
										else
											end = 0;
									}

									if (currentLine.StartsWith(
										"/*! Copyright (c) 2019 WhatsApp Inc. All Rights Reserved. */"))
									{
										return;
									}
								}
							}

							if (start)
								writer.WriteLine(currentLine);

							progress.Report((double) lineCount / lines);
							lineCount++;
						}
					}
				}
			}
		}


		private void ExtractDarkTheme(string darkThemePath)
		{
			var allText = new List<string>();
			if (File.Exists(darkThemePath))
			{
				using (var reader = new StreamReader(darkThemePath))
				{
					string currentLine;
					while ((currentLine = reader.ReadLine()) != null)
						allText.Add(currentLine);
				}
			} else
				Console.WriteLine("Error: File not found");

			if (File.Exists(resourcesPath + @"\css.css"))
			{
				using (var writer = new StreamWriter(new FileStream(resourcesPath + @"\css.css", FileMode.Append)))
				{
					foreach (string text in allText)
						writer.Write(text + "\n");
				}
			} else
				Console.WriteLine("Error: File not found");
		}

		private void Inject()
		{
			var readAllBytes = ReadBytes(asarPath);
			using (var fileStream = new FileStream(asarPath, FileMode.Open))
			{
				ModifyBytes(readAllBytes, "./cssm.css", @"..\css.css");
				foreach (byte character in readAllBytes)
				{
					fileStream.WriteByte(character);
				}
			}
		}

		private void ModifyBytes(IList<byte> bytes, string match, string replace)
		{
			var indexOfByte = new List<int>();
			var state = 0;
			using (var progressBar = new ProgressBar())
			{
				for (var m = 0; m < bytes.Count; m++)
				{
					byte _byte = bytes[m];
					var letter = Convert.ToChar(_byte);
					if (state < match.Length)
					{
						char character = match[state];
						if (letter == character)
						{
							indexOfByte.Add(m);
							state++;
						} else
						{
							indexOfByte.Clear();
							state = 0;
						}
					} else if (state == match.Length)
					{
						for (var i = 0; i < bytes.Count; i++)
						{
							for (var k = 0; k < indexOfByte.Count; k++)
							{
								if (i == indexOfByte[k])
								{
									bytes[i] = Convert.ToByte(replace[k]);
								}
							}
						}

						indexOfByte.Clear();
						state = 0;
					} else
					{
						state = 0;
					}

					progressBar.Report((double) m / (bytes.Count - 1));
				}
			}
		}

		private int LatestVersion(IEnumerable<string> directories)
		{
			var latestVersion = 0;
			foreach (string directory in directories)
			{
				if (directory != null)
				{
					if (Path.GetFileName(directory).StartsWith("app"))
					{
						string fileName = Path.GetFileName(directory);
						fileName = fileName.Replace("app-", "").Replace(".", "");
						if (fileName.Length == 5)
							fileName += 0;
						if (int.TryParse(fileName, out int version))
						{
							if (latestVersion < version)
							{
								latestVersion = version;
							}
						}
					}
				}
			}

			if (latestVersion % 10 == 0)
				latestVersion /= 10;
			return latestVersion;
		}

		private int GetLinesOfFile(string path)
		{
			var count = 0;
			using (var reader = new StreamReader(path))
			{
				while (reader.ReadLine() != null)
				{
					count++;
				}
			}

			return count;
		}

		private List<byte> ReadBytes(string path)
		{
			return new List<byte>(File.ReadAllBytes(path));
		}

		public override string ToString()
		{
			return "AppData path: " + whatsAppPath;
		}
	}
}